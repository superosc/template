#!/usr/bin/env bash

set -ex

# builds a package

source "$(dirname -- "${BASH_SOURCE[0]}")/vars.sh"

if [[ $1 == --test || $1 == -t ]]; then
  TESTMODE=true
  shift
else
  TESTMODE=false
fi

if [[ $# -ne 2 ]]; then
  echo >&2 "usage: $(basename "$0") [--test|-t] package version"
  exit 1
fi

if [[ ! -d $1 ]]; then
  echo >&2 "error: can't find package $1"
  exit 1
fi

if [[ ! -d "$1/debian" ]]; then
  echo >&2 "error: can't fine debian directory for $1"
  exit 1
fi

if [[ $(uname) == Linux ]]; then
  docker="sudo docker"
else
  docker=docker
fi

sdir="$(pwd)"
wdir="$(mktemp -d)"

cd "$1"
git archive --format tar.gz --prefix "$1-$2/" \
  --output "$wdir/$1_$2.orig.tar.gz" HEAD

cat << EOF > "$wdir/build.sh"
#!/usr/bin/env bash
set -e

apt-get update
apt-get install -y devscripts
cd /build
tar xzf $1_$2.orig.tar.gz
cd $1-$2
mk-build-deps --install \
  --tool 'apt-get -y -o Debug::pkgProblemResolver=yes --no-install-recommends'
rm *build-deps_*
debuild

exit 0
EOF

cd "$wdir"
$docker run -v "$(pwd):/build" --rm debian:$DEBIAN_RELEASE bash /build/build.sh

cd "$sdir"
cp "$wdir"/*.dsc "$wdir"/*.debian.tar.xz "$wdir"/*.orig.tar.gz .
if [[ $TESTMODE == true ]]; then
  cp "$wdir"/*.deb .
  find . -name '*-dbgsym_*.deb' -exec rm {} \;
fi

if [[ $(uname) == Linux ]]; then
  sudo rm -rf "$wdir"
else
  rm -rf "$wdir"
fi

exit 0

# vim: set ts=2 sw=2 et:
