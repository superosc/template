#!/usr/bin/env bash

set -ex

# publishes a package using the `osc` tool

source "$(dirname -- "${BASH_SOURCE[0]}")/vars.sh"

if [[ $# -ne 3 ]]; then
  echo >&2 "usage: $(basename "$0") project version release"
  exit 1
fi

if [[ ! -d $REPO_PREFIX ]]; then
  echo >&2 "error: you must checkout the repository first!"
  echo >&2 "error: run 'osc checkout $REPO_PREFIX'"
  exit 1
fi

cd "$1"
git tag -a "$2-1obs$3" -m "$1 $2-1~obs$3"
git push --follow-tags
cd ../

cd "$REPO_PREFIX"

if [[ ! -d $1 ]]; then
  osc mkpac "$1"
fi

cd "$1"

find . -maxdepth 1 \( \
  -name '*.debian.tar.xz' \
  -o -name '*.dsc' \
  -o -name '*.orig.tar.gz' \
  \) -exec osc rm {} \;

cp \
  "../../$1_$2-1~obs$3.debian.tar.xz" \
  "../../$1_$2-1~obs$3.dsc" \
  "../../$1_$2.orig.tar.gz" .

osc add \
  "./$1_$2-1~obs$3.debian.tar.xz" \
  "./$1_$2-1~obs$3.dsc" \
  "./$1_$2.orig.tar.gz"

osc commit -m "Add $1 $2-1~obs$3"

exit 0

# vim: set ts=2 sw=2 et:
