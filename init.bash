#!/usr/bin/env bash

set -ex

# initializes a repository with the initial debian packaging files

source "$(dirname -- "${BASH_SOURCE[0]}")/vars.sh"

if [[ $# -ne 2 ]]; then
  echo >&2 "usage: $(basename "$0") package version"
  exit 1
fi

if [[ ! -d $1 ]]; then
  echo >&2 "error: can't find package $1"
  exit 1
fi

if [[ -d "$1/debian" ]]; then
  echo >&2 "error: debian directory already exists in $1"
  exit 1
fi

if [[ $(uname) == Linux ]]; then
  docker="sudo docker"
else
  docker=docker
fi

sdir="$(pwd)"
wdir="$(mktemp -d)"

cd "$1"
git archive --format tar.gz --prefix "$1-$2/" -o "$wdir/$1-$2.tar.gz" HEAD

cat << EOF > "$wdir/init.sh"
#!/usr/bin/env bash
set -e

apt-get update
apt-get install -y devscripts debmake
cd /build
tar xzf $1-$2.tar.gz
cd $1-$2
debmake

exit 0
EOF

cd "$wdir"
$docker run -v "$(pwd):/build" --rm debian:$DEBIAN_RELEASE bash /build/init.sh

cd "$sdir/$1"
cp -R "$wdir/$1-$2/debian" .

if [[ $(uname) == Linux ]]; then
  sudo rm -rf "$wdir"
else
  rm -rf "$wdir"
fi

exit 0

# vim: set ts=2 sw=2 et:
