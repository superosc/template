#!/usr/bin/env bash

mkdir -p ~/.config/osc
cat <<EOF > ~/.config/osc/oscrc
[general]
apiurl=https://api.opensuse.org

[https://api.opensuse.org]
user=$OPENBUILDSERVICE_USERNAME
pass=$OPENBUILDSERVICE_PASSWORD
credentials_mgr_class=osc.credentials.PlaintextConfigFileCredentialsManager
EOF
