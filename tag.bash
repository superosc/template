#!/usr/bin/env bash

set -ex

# generates the correct git tag based on package name and version

if [[ $# -ne 0 ]]; then
  echo >&2 "usage: $(basename "$0")"
  exit 1
fi

if [[ ! -d debian ]]; then
  echo >&2 "error: run this from the directory of the package to tag!"
  exit 1
fi

PKGVER="$(head -n 1 debian/changelog  | awk -F\( '{print $2}' | \
  awk -F\) '{print $1}')"

git tag -a "${PKGVER//\~/}" -m "$(basename "$(pwd)") $PKGVER"
git push --follow-tags

exit 0

# vim: set ts=2 sw=2 et:
