# supermario debian repository

Instructions and scripts for operating my custom repository hosted on the
[openSUSE Open Build
Service](https://build.opensuse.org/project/show/home:mfinelli:supermario)

Start by checking out the repository locally: `osc checkout
home:mfinelli:supermario`.

## adding new packages

To create a new package, create a new repository under the `superosc` group on
Gitlab with the same name of the eventual package.

Get a release tarball and unpack it into the new repository (we won't be
tracking all of the changes, just overwriting the contents of the repository
on each release that we want to publish).

```shell
cd $project
tar xf ./path/to/project-version.tar.gz --strip-components=1
git commit -am "Add $project $version"
git push
```

Then from the parent directory (i.e., where we have all of the package
repositories checked out, run the `init.bash` script to initialize all of the
files necessary for creating a debian package.

```shell
./mgmt/init.bash $project $version
cd $project
git add debian
git commit -m "Generate debian package files"
git push
```

Now, make all of the necessary changes to the `debian` files in order to get
the package to build correctly. Once done, commit the changes. And run a build
to generate the necessary files to publish the package.

```shell
git add debian
git commit -m "Update package files"
git push
cd ../
./mgmt/build.bash $project $version
```

If you need to run and inspect the build to configure everything as desired
keep a single "WIP" commit (keep `git commit --amend` as needed and don't push
it until finished) and run the build script with the `--test` flag:

```shell
git commit --amend -am WIP
cd ../
./mgmt/build --test $project $version
```

When finished, update the WIP commit with a real commit message and then
proceed as normal.

## publishing packages

Publishing is done by a CI pipeline, triggered by pushing a git tag. Tag the
repository (from within the repository you want to publish):

```shell
../mgmt/tag.bash
```

If the build is successful a new release will be published on the Gitlab
project page and the artifacts will be pushed to the Open Build Service (where
they will be built and published for all configured architectures).

### repository configuration

Under each project repository CI/CD settings set the CI/CD configuration file
to `ci/gitlab-ci.yml@supermario-pkgs/MGMT`.

The OBS username/password environment variables are only exposed to protected
branches and tags so you must add a wildcard (`*`) protected tag so that assets
can be pushed to the build service.

## updating packages

TODO: document updating the changelog with `dch -v newversion-1` (from
`devscripts` installed)
